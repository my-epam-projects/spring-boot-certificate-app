package com.epam.esm.utils;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * Represents a paginated response containing the total count of results and the actual result list.
 *
 * @param <E> The type of the result list.
 * @author Jaloliddinov
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PageRequest<E> {

    @JsonProperty("result_count")
    private Integer resultCount;

    @JsonProperty("resultList")
    private E resultList;
}
