package com.epam.esm.repository;


import com.epam.esm.entity.User;
import com.epam.esm.utils.PageRequest;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.*;
import org.springframework.stereotype.Repository;

import java.util.List;
/**
 * Repository class for managing operations related to users.
 * Handles CRUD operations, searching, and pagination.
 *
 * @author Jaloliddinov
 */
@Repository
public class UserRepository {
    @PersistenceContext
    private EntityManager entityManager;


    public PageRequest<List<User>> findAll(Integer page, Integer size, String sortField, String sortOrder, String filterField, String filterValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
        Root<User> root = criteriaQuery.from(User.class);

        // Select
        criteriaQuery.select(root);

        // Where clause
        Predicate filterPredicate = buildFilterPredicate(criteriaBuilder, root, filterField, filterValue);
        if (filterPredicate != null) {
            criteriaQuery.where(filterPredicate);
        }

        if (sortField != null && !sortField.isEmpty()) {
            Order order = sortOrder.equalsIgnoreCase("desc") ?
                    criteriaBuilder.desc(root.get(sortField)) :
                    criteriaBuilder.asc(root.get(sortField));
            criteriaQuery.orderBy(order);
        }

        TypedQuery<User> typedQuery = entityManager.createQuery(criteriaQuery);

        // Apply pagination
        List<User> resultList = applyPagination(typedQuery, page, size);

        return new PageRequest<>(resultList.size(), resultList);
    }

    public User findById(Long id) {
        return entityManager.find(User.class, id);
    }

    public User findByUsername(String username) {
        try {
            return entityManager.createQuery("SELECT t FROM User t WHERE t.username = :username",
                    User.class).setParameter("username", username).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }

    }


    public void save(User user) {
        entityManager.persist(user);
    }


    public User findByEmail(String email) {
        try {
            return entityManager.createQuery("SELECT t FROM User t WHERE t.email = :email",
                    User.class).setParameter("email", email).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public User findByPassword(String password) {
        try {
            return entityManager.createQuery("SELECT t FROM User t WHERE t.password = :password",
                    User.class).setParameter("password", password).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }

    }

    private Predicate buildFilterPredicate(CriteriaBuilder criteriaBuilder, Root<User> root, String filterField, String filterValue) {
        if (isValidFilter(filterField, filterValue)) {
            return criteriaBuilder.equal(root.get(filterField), filterValue);
        }
        return null;
    }

    private boolean isValidFilter(String filterField, String filterValue) {
        return filterField != null && filterValue != null && !filterField.isEmpty() && !filterValue.isEmpty();
    }

    private List<User> applyPagination(TypedQuery<User> typedQuery, Integer page, Integer size) {
        if (page != null && size != null) {
            typedQuery.setFirstResult((page - 1) * size);
            typedQuery.setMaxResults(size);
        }
        return typedQuery.getResultList();
    }


    public User randomUser() {
        return entityManager.createQuery("select t from User t ORDER BY RANDOM() LIMIT 1", User.class).getSingleResult();
    }
}
