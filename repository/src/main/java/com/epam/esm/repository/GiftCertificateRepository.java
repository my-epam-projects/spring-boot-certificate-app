package com.epam.esm.repository;


import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.entity.Tag;
import com.epam.esm.utils.PageRequest;
import com.github.javafaker.Faker;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.*;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
/**
 * Repository class for managing operations related to gift certificates.
 * Handles CRUD operations, searching, and pagination.
 *
 * @author Jaloliddinov
 */
@Repository
public class GiftCertificateRepository {

    @PersistenceContext
    private EntityManager entityManager;


    public PageRequest<List<GiftCertificate>> findAll(Integer page, Integer size, String sortField, String sortOrder, String filterField, String filterValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<GiftCertificate> criteriaQuery = criteriaBuilder.createQuery(GiftCertificate.class);
        Root<GiftCertificate> root = criteriaQuery.from(GiftCertificate.class);

        if (filterField != null && filterValue != null && !filterField.isEmpty() && !filterValue.isEmpty()) {
            Predicate filterPredicate = criteriaBuilder.equal(root.get(filterField), filterValue);
            criteriaQuery.where(filterPredicate);
        }

        if (sortField != null && !sortField.isEmpty()) {
            Order order = sortOrder.equalsIgnoreCase("desc") ?
                    criteriaBuilder.desc(root.get(sortField)) :
                    criteriaBuilder.asc(root.get(sortField));
            criteriaQuery.orderBy(order);
        }

        TypedQuery<GiftCertificate> typedQuery = entityManager.createQuery(criteriaQuery);

        applyPagination(typedQuery,page,size);


        return new PageRequest<>(typedQuery.getResultList().size(), typedQuery.getResultList());
    }


    public PageRequest<List<GiftCertificate>> getByTags(Set<String> tagNames, Integer page, Integer size, String sortField, String sortOrder, String filterField, String filterValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<GiftCertificate> criteriaQuery = criteriaBuilder.createQuery(GiftCertificate.class);
        Root<GiftCertificate> root = criteriaQuery.from(GiftCertificate.class);

        // Join tags with left join fetch
        root.fetch("tags", JoinType.LEFT);

        // Add a predicate for filtering by tag names
        Predicate tagPredicate = root.join("tags").get("name").in(tagNames);
        criteriaQuery.where(tagPredicate);

        // Add additional predicates for optional filtering
        if (filterField != null && filterValue != null && !filterField.isEmpty() && !filterValue.isEmpty()) {
            Predicate filterPredicate = criteriaBuilder.equal(root.get(filterField), filterValue);
            criteriaQuery.where(filterPredicate, tagPredicate);
        }

        if (sortField != null && !sortField.isEmpty()) {
            Order order = sortOrder.equalsIgnoreCase("desc") ?
                    criteriaBuilder.desc(root.get(sortField)) :
                    criteriaBuilder.asc(root.get(sortField));
            criteriaQuery.orderBy(order);
        }

        TypedQuery<GiftCertificate> typedQuery = entityManager.createQuery(criteriaQuery);

        // Apply pagination
        applyPagination(typedQuery,page,size);

        return new PageRequest<>(typedQuery.getResultList().size(), typedQuery.getResultList());
    }

    public PageRequest<List<GiftCertificate>> getAllByName(String name, Integer page, Integer size, String sortField, String sortOrder, String filterField, String filterValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<GiftCertificate> criteriaQuery = criteriaBuilder.createQuery(GiftCertificate.class);
        Root<GiftCertificate> root = criteriaQuery.from(GiftCertificate.class);

        // Add a predicate for filtering by name
        Predicate namePredicate = criteriaBuilder.like(root.get("name"), "%" + name + "%");
        criteriaQuery.where(namePredicate);

        // Add additional predicates for optional filtering
        if (filterField != null && filterValue != null && !filterField.isEmpty() && !filterValue.isEmpty()) {
            Predicate filterPredicate = criteriaBuilder.equal(root.get(filterField), filterValue);
            criteriaQuery.where(filterPredicate, namePredicate);
        }

        // Add order by based on sort field
        if (sortField != null && !sortField.isEmpty()) {
            Order order = sortOrder.equalsIgnoreCase("desc") ?
                    criteriaBuilder.desc(root.get(sortField)) :
                    criteriaBuilder.asc(root.get(sortField));
            criteriaQuery.orderBy(order);
        }

        TypedQuery<GiftCertificate> typedQuery = entityManager.createQuery(criteriaQuery);

        // Apply pagination
       applyPagination(typedQuery,page,size);

        return new PageRequest<>(typedQuery.getResultList().size(), typedQuery.getResultList());
    }

    public GiftCertificate findById(Long id) {
        return entityManager.find(GiftCertificate.class, id);
    }

    public void save(GiftCertificate certificate) {
        entityManager.persist(certificate);
    }

    public void update(GiftCertificate certificate) {
        entityManager.merge(certificate);
    }

    public void delete(GiftCertificate certificate) {
        entityManager.remove(certificate);
    }

    public void removeTags(GiftCertificate giftCertificate, Set<Tag> tagList) {
        Set<Tag> giftCertificateTags = giftCertificate.getTags();
        giftCertificateTags.removeAll(tagList);
        entityManager.merge(giftCertificate);
    }

    public void generateAndSave(Set<Tag> tagList) {
        Faker faker = new Faker();
        for (int i = 0; i < 10000; i++) {
            GiftCertificate giftCertificate = new GiftCertificate();

            giftCertificate.setName(String.join(" ", faker.lorem().words(2)));
            giftCertificate.setDescription(faker.lorem().sentence());
            giftCertificate.setPrice(faker.number().randomDigitNotZero() * 10); // Adjust as needed
            giftCertificate.setDuration(faker.number().numberBetween(1, 365)); // Adjust as needed
            giftCertificate.setCreateDate(LocalDateTime.now());
            giftCertificate.setLastUpdateDate(LocalDateTime.now());


            giftCertificate.setTags(tagList);
            save(giftCertificate);
        }
    }

    public GiftCertificate getRandomCertificate() {
        return entityManager.createQuery("select t from GiftCertificate t order by RANDOM() LIMIT 1",
                GiftCertificate.class).getSingleResult();
    }

    private void applyPagination(TypedQuery<?> typedQuery, Integer page, Integer size) {
        if (page != null && size != null) {
            typedQuery.setFirstResult((page - 1) * size);
            typedQuery.setMaxResults(size);
        }
    }
}
