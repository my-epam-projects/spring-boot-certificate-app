package com.epam.esm.repository;

import com.epam.esm.entity.Role;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
/**
 * Repository class for managing operations related to roles.
 * Handles only READ operations
 *
 * @author Jaloliddinov
 */
@Repository
public class RoleRepository {

    @PersistenceContext
    private EntityManager entityManager;
    public Role findById(Long id){
        try {
            return entityManager.createQuery("SELECT t FROM Role t WHERE t.id = :id",Role.class).setParameter("id",id).getSingleResult();
        }catch (NoResultException e){
            return null;
        }
    }
}
