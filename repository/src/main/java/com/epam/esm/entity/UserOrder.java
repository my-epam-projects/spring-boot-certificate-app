package com.epam.esm.entity;

import com.epam.esm.base.BaseEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.Audited;

import java.time.LocalDateTime;

/**
 * Represents a user order entity in the system.
 * Extends the {@link BaseEntity} class to inherit ID properties.
 *
 * @author Jaloliddinov
 */
@Getter
@Setter
@Entity
@Audited
@Table(name = "user_orders")
@AllArgsConstructor
@NoArgsConstructor
public class UserOrder extends BaseEntity {


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gift_certificate_id", referencedColumnName = "id")
    private GiftCertificate giftCertificate;


    @Column(name = "cost")
    private Integer cost;

    @Column(name = "purchase_time")
    private LocalDateTime purchaseTime = LocalDateTime.now();
}
