package com.epam.esm.base;

public interface BaseURI {

    String API = "/api";
    String V1 = "/v1";


    String USERS = "/users";
    String GIFT_CERTIFICATES = "/gift_certificates";
    String TAGS = "/tags";
    String USER_ORDERS = "/user_orders";
    String PUBLIC = "/public";

    String LOGIN = "/login";
    String REGISTRATION = "/registration";
    String REFRESH = "/refresh";
    String TOKEN = "/token";

    String BY_NAME = "/by_name";
    String BY_USER = "/by_user";
    String SEARCH_BY_TAGS = "search_by_tags";


}
