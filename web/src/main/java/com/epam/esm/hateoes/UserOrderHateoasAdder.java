package com.epam.esm.hateoes;

import com.epam.esm.controller.UserOrderController;
import com.epam.esm.dto.user_order.UserOrderCreateDTO;
import com.epam.esm.dto.user_order.UserOrderDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class UserOrderHateoasAdder implements HateoasAssembler<UserOrderDTO> {
    private final UserHateoasAdder userHateoasAdder;
    private final GiftCertificateHateoasAdder certificateHateoasAdder;


    @Override
    public void addLinks(UserOrderDTO dto) {
        dto.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(UserOrderController.class).getById(dto.getId())).withSelfRel());
        dto.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(UserOrderController.class).create(new UserOrderCreateDTO())).withRel("create"));
        userHateoasAdder.addLinks(dto.getUser());
        certificateHateoasAdder.addLinks(dto.getCertificate());
    }

    @Override
    public void addLinks(List<UserOrderDTO> dto) {
        dto.forEach(this::addLinks);
    }
}
