package com.epam.esm.hateoes;

import com.epam.esm.controller.GiftCertificateController;
import com.epam.esm.dto.gift_certificate.GiftCertificateCreateDTO;
import com.epam.esm.dto.gift_certificate.GiftCertificateDTO;
import com.epam.esm.dto.gift_certificate.GiftCertificateUpdateDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class GiftCertificateHateoasAdder implements HateoasAssembler<GiftCertificateDTO> {

    private final TagHateoasAdder tagHateoasAdder;

    @Override
    public void addLinks(GiftCertificateDTO dto) {
        dto.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(GiftCertificateController.class).getById(dto.getId())).withSelfRel());
        dto.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(GiftCertificateController.class).create(new GiftCertificateCreateDTO())).withRel("create"));
        dto.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(GiftCertificateController.class).update(dto.getId(), new GiftCertificateUpdateDTO())).withRel("update"));
        dto.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(GiftCertificateController.class).delete(dto.getId())).withRel("delete"));
        tagHateoasAdder.addLinks(dto.getTagList());
    }

    @Override
    public void addLinks(List<GiftCertificateDTO> dto) {
        dto.forEach(this::addLinks);
    }
}
