package com.epam.esm.controller;

import com.epam.esm.base.BaseURI;
import com.epam.esm.common.ResponseData;
import com.epam.esm.dto.tag.TagWithTotalCostDTO;
import com.epam.esm.dto.user_order.UserOrderCreateDTO;
import com.epam.esm.dto.user_order.UserOrderDTO;
import com.epam.esm.exceptions.AlreadyExistsException;
import com.epam.esm.exceptions.CustomNotFoundException;
import com.epam.esm.hateoes.UserOrderHateoasAdder;
import com.epam.esm.service.UserOrderService;
import com.epam.esm.utils.PageRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(BaseURI.API + BaseURI.V1 + BaseURI.USER_ORDERS)
public class UserOrderController {

    private final UserOrderService userOrderService;
    private final UserOrderHateoasAdder hateoasAdder;

    @Operation(summary = "Get by ID", description = "Get by ID")
    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @GetMapping("/{id}")
    public ResponseEntity<ResponseData<UserOrderDTO>> getById(@PathVariable(value = "id") Long id) throws CustomNotFoundException, AlreadyExistsException {
        UserOrderDTO dto = userOrderService.getById(id);
        hateoasAdder.addLinks(dto);
        return ResponseData.success200(dto);
    }


    @Operation(summary = "Get by user", description = "Get by user")
    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @GetMapping(BaseURI.BY_USER + "/{id}")
    public ResponseEntity<ResponseData<PageRequest<List<UserOrderDTO>>>> getByUser(@PathVariable(value = "id") Long userId,
                                                                                   @RequestParam(value = "page", defaultValue = "1") Integer page,
                                                                                   @RequestParam(value = "size", defaultValue = "10") Integer size,
                                                                                   @RequestParam(value = "sort", defaultValue = "id") String sortField,
                                                                                   @RequestParam(value = "order", defaultValue = "asc") String sortOrder,
                                                                                   @RequestParam(value = "filter", required = false) String filterField,
                                                                                   @RequestParam(value = "value", required = false) String filterValue) throws CustomNotFoundException, AlreadyExistsException {
        PageRequest<List<UserOrderDTO>> pageRequest = userOrderService.getByUser(userId, page, size, sortField, sortOrder, filterField, filterValue);
        hateoasAdder.addLinks(pageRequest.getResultList());
        return ResponseData.success200(pageRequest);
    }

    @Operation(summary = "Get most used tag", description = "Get most used tag")
    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @GetMapping("/{userId}/popular-tag-highest-cost")
    public ResponseEntity<ResponseData<List<TagWithTotalCostDTO>>> getHighestPriceAndMostUsedTagOfUser(@PathVariable(value = "userId") Long userId) throws CustomNotFoundException, AlreadyExistsException {
        List<TagWithTotalCostDTO> dtoList = userOrderService.getHighestPriceAndMostUsedTagOfUser(userId);
        return ResponseData.success200(dtoList);
    }

    @Operation(summary = "Create Orders", description = "Create Orders")
    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @PostMapping
    public ResponseEntity<ResponseData<UserOrderDTO>> create(@RequestBody @Valid UserOrderCreateDTO dto) throws CustomNotFoundException, AlreadyExistsException {
        UserOrderDTO orderDTO = userOrderService.create(dto);
        hateoasAdder.addLinks(orderDTO);
        return ResponseData.success201(orderDTO);
    }
}
