package com.epam.esm.controller;

import com.epam.esm.base.BaseURI;
import com.epam.esm.common.ResponseData;
import com.epam.esm.dto.token.ReqLoginDTO;
import com.epam.esm.dto.token.ReqRefreshTokenDTO;
import com.epam.esm.dto.token.SessionDTO;
import com.epam.esm.dto.user.UserCreateDTO;
import com.epam.esm.dto.user.UserDTO;
import com.epam.esm.hateoes.HateoasAssembler;
import com.epam.esm.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(BaseURI.API + BaseURI.V1 + BaseURI.PUBLIC)
public class AuthController {

    private final UserService userService;
    private final HateoasAssembler<UserDTO> hateoasAssembler;

    @PostMapping(BaseURI.LOGIN)
    public ResponseEntity<ResponseData<SessionDTO>> login(@RequestBody @Valid ReqLoginDTO dto) {
        SessionDTO session = userService.login(dto);
        hateoasAssembler.addLinks(session.getUser());
        return ResponseData.success200(session);
    }

    @PostMapping(BaseURI.REGISTRATION)
    public ResponseEntity<ResponseData<SessionDTO>> register(@RequestBody @Valid UserCreateDTO dto) {
        SessionDTO session = userService.register(dto);
        hateoasAssembler.addLinks(session.getUser());
        return ResponseData.success201(session);
    }

    @PostMapping(BaseURI.REFRESH + BaseURI.TOKEN)
    public ResponseEntity<ResponseData<SessionDTO>> refreshToken(@RequestBody @Valid ReqRefreshTokenDTO dto) {
        SessionDTO session = userService.refreshToken(dto);
        hateoasAssembler.addLinks(session.getUser());
        return ResponseData.success200(session);
    }
}
