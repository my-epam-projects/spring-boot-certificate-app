package com.epam.esm.mapper;

import com.epam.esm.dto.gift_certificate.GiftCertificateCreateDTO;
import com.epam.esm.dto.gift_certificate.GiftCertificateDTO;
import com.epam.esm.dto.gift_certificate.GiftCertificateUpdateDTO;
import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.helper.Utils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring", uses = {TagMapper.class, Utils.class})
public interface GiftCertificateMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "price", source = "price")
    @Mapping(target = "duration", source = "duration")
    @Mapping(target = "createDate", source = "createDate")
    @Mapping(target = "lastUpdateDate", source = "lastUpdateDate")
    @Mapping(target = "tagList", source = "tags")
    GiftCertificateDTO toDto(GiftCertificate certificate);

    List<GiftCertificateDTO> toDtoList(List<GiftCertificate> certificateList);

    @Mapping(target = "name", source = "dto.name")
    @Mapping(target = "description", source = "dto.description")
    @Mapping(target = "price", source = "dto.price")
    @Mapping(target = "duration", source = "dto.duration")
    GiftCertificate toEntity(GiftCertificateCreateDTO dto);

    @Mapping(target = "name", source = "dto.name")
    @Mapping(target = "description", source = "dto.description")
    @Mapping(target = "price", source = "dto.price")
    @Mapping(target = "duration", source = "dto.duration")
    GiftCertificate toEntity(@MappingTarget GiftCertificate certificate, GiftCertificateUpdateDTO dto);
}
