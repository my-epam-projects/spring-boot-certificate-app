package com.epam.esm.service;

import com.epam.esm.dto.token.SessionDTO;
import io.jsonwebtoken.Claims;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Date;
import java.util.Map;
import java.util.function.Function;

/**
 * Service interface for handling JSON Web Tokens (JWT).
 * Defines methods for token extraction, validation, generation, and token refreshing.
 *
 * @author Jaloliddinov
 */
public interface JwtService {

    /**
     * Extracts the username from a JWT.
     *
     * @param token The JWT from which to extract the username.
     * @return The username extracted from the JWT.
     */
    String extractUsername(String token);

    /**
     * Extracts the expiration date from a JWT.
     *
     * @param token The JWT from which to extract the expiration date.
     * @return The expiration date extracted from the JWT.
     */
    Date extractExpiration(String token);

    /**
     * Extracts a claim from a JWT using a provided claims resolver function.
     *
     * @param <T>            The type of the claim.
     * @param token          The JWT from which to extract the claim.
     * @param claimsResolver The function to resolve the claim from the JWT's claims.
     * @return The extracted claim.
     */
    <T> T extractClaim(String token, Function<Claims, T> claimsResolver);

    /**
     * Checks if a JWT is valid for a given user.
     *
     * @param token       The JWT to validate.
     * @param userDetails The UserDetails of the user for validation.
     * @return True if the token is valid, false otherwise.
     */
    boolean isTokenValid(String token, UserDetails userDetails);

    /**
     * Generates an access token for a given user.
     *
     * @param userDetails The UserDetails of the user for whom to generate the access token.
     * @return The generated access token.
     */
    String generateAccessToken(UserDetails userDetails);

    /**
     * Generates a refresh token for a given user.
     *
     * @param userDetails The UserDetails of the user for whom to generate the refresh token.
     * @return The generated refresh token.
     */
    String generateRefreshToken(UserDetails userDetails);

    /**
     * Refreshes an access token using a refresh token.
     *
     * @param refreshToken The refresh token to use for token refreshing.
     * @return A map containing the new session details and the updated access token.
     */
    Map<SessionDTO, String> refreshToken(String refreshToken);
}
