package com.epam.esm.service;

import com.epam.esm.entity.Role;

/**
 * Service interface for managing user roles.
 * Defines a method for retrieving a role by its ID.
 *
 * @author Jaloliddinov
 */
public interface RoleService {

    /**
     * Retrieves a role by its ID.
     *
     * @param id The ID of the role.
     * @return The role with the specified ID.
     */
    Role getById(Long id);
}
