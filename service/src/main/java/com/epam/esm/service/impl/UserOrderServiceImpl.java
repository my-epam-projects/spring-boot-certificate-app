package com.epam.esm.service.impl;

import com.epam.esm.dto.tag.TagWithTotalCostDTO;
import com.epam.esm.dto.user_order.UserOrderCreateDTO;
import com.epam.esm.dto.user_order.UserOrderDTO;
import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.entity.User;
import com.epam.esm.entity.UserOrder;
import com.epam.esm.exceptions.CustomNotFoundException;
import com.epam.esm.helper.UserSession;
import com.epam.esm.helper.Utils;
import com.epam.esm.mapper.UserOrderMapper;
import com.epam.esm.repository.UserOrderRepository;
import com.epam.esm.service.GiftCertificateService;
import com.epam.esm.service.TagService;
import com.epam.esm.service.UserOrderService;
import com.epam.esm.service.UserService;
import com.epam.esm.utils.PageRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserOrderServiceImpl implements UserOrderService {

    private final UserOrderRepository userOrderRepository;
    private final UserOrderMapper orderMapper;
    private final UserService userService;
    private final GiftCertificateService certificateService;
    private final TagService tagService;
    private final UserSession userSession;

    @Override
    public UserOrderDTO getById(Long id) {
        UserOrder userOrder = finById(id);
        return orderMapper.toDto(userOrder);
    }


    @Override
    public PageRequest<List<UserOrderDTO>> getByUser(Long userId, Integer page, Integer size, String sortField, String sortOrder, String filterField, String filterValue) {
        if (!"asc".equalsIgnoreCase(sortOrder) && !"desc".equalsIgnoreCase(sortOrder)) {
            throw new IllegalArgumentException("Invalid sortOrder. Use 'asc' or 'desc'.");
        }
        User user = userService.findById(userId);
        PageRequest<List<UserOrder>> pageRequest = userOrderRepository.findByUser(user.getId(), page, size, sortField, sortOrder, filterField, filterValue);
        if (Utils.isEmpty(pageRequest.getResultList())) {
            throw new CustomNotFoundException("Orders are not found for User ID: " + userId);
        }
        List<UserOrderDTO> dtoList = orderMapper.toDtoList(pageRequest.getResultList());
        return new PageRequest<>(pageRequest.getResultCount(), dtoList);
    }

    @Override
    public List<TagWithTotalCostDTO> getHighestPriceAndMostUsedTagOfUser(Long userId) {
        User user = userService.findById(userId);
        List<UserOrder> orderList = userOrderRepository.finByUser(userId);
        if (Utils.isEmpty(orderList)) {
            throw new CustomNotFoundException("orders are not found for User ID: " + userId);
        }
        return tagService.getMostWidelyUsedTagOfUser(user.getId());
    }

    @Override
    @Transactional
    public UserOrderDTO create(UserOrderCreateDTO dto) {
        User user = userSession.getUser();
        GiftCertificate certificate = certificateService.findById(dto.getCertificateId());

        UserOrder userOrder = new UserOrder();
        userOrder.setUser(user);
        userOrder.setGiftCertificate(certificate);
        userOrder.setCost(certificate.getPrice());
        userOrder.setPurchaseTime(LocalDateTime.now());

        userOrderRepository.save(userOrder);

        return orderMapper.toDto(userOrder);
    }

    @Transactional
    public void generateAndSave() {
        for (int i = 0; i < 1000; i++) {
            GiftCertificate certificate = certificateService.getRandomCertificate();
            User user = userService.getRandomUser();
            UserOrder userOrder = new UserOrder();
            userOrder.setUser(user);
            userOrder.setGiftCertificate(certificate);
            userOrder.setCost(certificate.getPrice());
            userOrder.setPurchaseTime(LocalDateTime.now());
            userOrderRepository.save(userOrder);

        }
    }

    private UserOrder finById(Long id) {
        UserOrder userOrder = userOrderRepository.findById(id);
        if (Utils.isEmpty(userOrder)) {
            throw new CustomNotFoundException("User Order with ID: " + id + " is not found");
        }
        return userOrder;
    }

}
