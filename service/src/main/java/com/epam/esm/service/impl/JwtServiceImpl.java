package com.epam.esm.service.impl;

import com.epam.esm.dto.token.SessionDTO;
import com.epam.esm.exceptions.InValidInputException;
import com.epam.esm.service.JwtService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
public class JwtServiceImpl implements JwtService {

    private final String SECRET_KEY = "3D2B7E151628AED2A6ABF7158809CF4F3C76E22D36F5D9E4C5F3C311B4A8A62F";
    private final long expirationAccessToken = System.currentTimeMillis() + 86400000L; // 1  day
    private final long expirationRefreshToken = System.currentTimeMillis() + 86400000L * 2; // 2  day
    private final UserDetailsService userDetailsService;

    public JwtServiceImpl(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }


    @Override
    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }


    @Override
    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts
                .parser()
                .setSigningKey(getSignKey())
                .build()
                .parseClaimsJws(token)
                .getPayload();
    }


    @Override
    public boolean isTokenValid(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    @Override
    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    @Override
    public String generateRefreshToken(UserDetails userDetails) {
        return this.buildJwt(userDetails, expirationRefreshToken,"refresh");
    }

    @Override
    public String generateAccessToken(UserDetails userDetails) {
        return this.buildJwt(userDetails, expirationAccessToken,"access");
    }


    private String buildJwt(UserDetails userDetails, long expiration, String tokenType) {
        Date now = new Date();
        return Jwts.builder()
                .claim("token_type", tokenType)
//                .claims(extraClaims)
                .subject(userDetails.getUsername())
                .issuedAt(now)
                .expiration(new Date(expiration))
                .signWith(getSignKey(), SignatureAlgorithm.HS256)
                .compact();
    }

    @Override
    public Map<SessionDTO, String> refreshToken(String refreshToken) {
        Map<SessionDTO, String> result = new HashMap<>();
        final String username = extractUsername(refreshToken);
        if (username == null) {
            throw new InValidInputException("token is not valid");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        if (!isTokenValid(refreshToken, userDetails)) {
            throw new InValidInputException("token is not valid");
        }
        String accessToken = generateAccessToken(userDetails);
        String newRefreshToken = generateRefreshToken(userDetails);
        SessionDTO dto = SessionDTO.builder()
                .accessToken(accessToken)
                .refreshToken(newRefreshToken)
                .build();

        result.put(dto, userDetails.getUsername());
        return result;
    }

    private Key getSignKey() {
        byte[] keyBytes = Decoders.BASE64.decode(SECRET_KEY);
        return Keys.hmacShaKeyFor(keyBytes);
    }


}
