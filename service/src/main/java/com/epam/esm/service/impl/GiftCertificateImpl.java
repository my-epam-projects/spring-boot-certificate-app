package com.epam.esm.service.impl;


import com.epam.esm.dto.gift_certificate.GiftCertificateCreateDTO;
import com.epam.esm.dto.gift_certificate.GiftCertificateDTO;
import com.epam.esm.dto.gift_certificate.GiftCertificateUpdateDTO;
import com.epam.esm.dto.gift_certificate_tag.GiftCertificateRequestDTO;
import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.entity.Tag;
import com.epam.esm.exceptions.AlreadyExistsException;
import com.epam.esm.exceptions.CustomNotFoundException;
import com.epam.esm.exceptions.InValidInputException;
import com.epam.esm.helper.Utils;
import com.epam.esm.mapper.GiftCertificateMapper;
import com.epam.esm.repository.GiftCertificateRepository;
import com.epam.esm.service.GiftCertificateService;
import com.epam.esm.service.TagService;
import com.epam.esm.utils.PageRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class GiftCertificateImpl implements GiftCertificateService {

    private final GiftCertificateRepository certificateRepository;

    private final GiftCertificateMapper certificateMapper;
    private final TagService tagService;

    @Override
    public PageRequest<List<GiftCertificateDTO>> getAll(Integer page, Integer size, String sortField, String sortOrder, String filterField, String filterValue) {
        checkSortOrder(sortOrder);

        PageRequest<List<GiftCertificate>> pageRequest = certificateRepository.findAll(page, size, sortField, sortOrder, filterField, filterValue);
        List<GiftCertificateDTO> dtoList = checkCertificates(pageRequest.getResultList());
        return new PageRequest<>(pageRequest.getResultCount(), dtoList);
    }

    @Override
    public GiftCertificateDTO getById(Long id) {
        GiftCertificate certificate = findById(id);
        return certificateMapper.toDto(certificate);
    }

    @Override
    public PageRequest<List<GiftCertificateDTO>> getAllByName(String name, Integer page, Integer size, String sortField, String sortOrder, String filterField, String filterValue) {
        checkSortOrder(sortOrder);

        if (Utils.isEmpty(name)) {
            throw new InValidInputException("name cannot be empty");
        }
        PageRequest<List<GiftCertificate>> pageRequest = certificateRepository.getAllByName(name, page, size, sortField, sortOrder, filterField, filterValue);
        List<GiftCertificateDTO> dtoList = checkCertificates(pageRequest.getResultList());
        return new PageRequest<>(pageRequest.getResultCount(), dtoList);
    }

    @Override
    public PageRequest<List<GiftCertificateDTO>> getByTags(Set<String> tagNameList, Integer page, Integer size, String sortField, String sortOrder, String filterField, String filterValue) {
        checkSortOrder(sortOrder);

        PageRequest<List<GiftCertificate>> pageRequest = certificateRepository.getByTags(tagNameList, page, size, sortField, sortOrder, filterField, filterValue);
        if (Utils.isEmpty(pageRequest.getResultList())) {
            throw new CustomNotFoundException("Certificates are not found for given Tags: " + tagNameList);
        }
        List<GiftCertificateDTO> dtoList = certificateMapper.toDtoList(pageRequest.getResultList());
        return new PageRequest<>(pageRequest.getResultCount(), dtoList);
    }

    @Override
    @Transactional
    public GiftCertificateDTO create(GiftCertificateCreateDTO dto) {
        Set<Tag> tagList = tagService.getTagsByIds(dto.getTagIds());
        GiftCertificate certificate = certificateMapper.toEntity(dto);
        certificate.setCreateDate(LocalDateTime.now());
        certificate.setLastUpdateDate(LocalDateTime.now());
        certificate.setTags(tagList);

        certificateRepository.save(certificate);

        return certificateMapper.toDto(certificate);
    }

    @Override
    @Transactional
    public GiftCertificateDTO update(Long id, GiftCertificateUpdateDTO dto) {
        GiftCertificate certificate = findById(id);
        GiftCertificate certificateToUpdate = certificateMapper.toEntity(certificate, dto);
        certificate.setLastUpdateDate(LocalDateTime.now());
        certificateRepository.update(certificateToUpdate);
        return certificateMapper.toDto(certificateToUpdate);
    }

    @Override
    @Transactional
    public GiftCertificateDTO updateOnlyPrice(Long id, Integer price) {
        GiftCertificate certificate = findById(id);
        certificate.setPrice(price);
        certificate.setLastUpdateDate(LocalDateTime.now());
        certificateRepository.update(certificate);
        return certificateMapper.toDto(certificate);
    }

    @Override
    @Transactional
    public Boolean delete(Long id) throws CustomNotFoundException {
        GiftCertificate certificate = findById(id);
        certificateRepository.delete(certificate);
        return true;
    }

    @Override
    @Transactional
    public GiftCertificateDTO addNewTags(GiftCertificateRequestDTO dto) {
        Set<Tag> tagList = tagService.getTagsByIds(new ArrayList<>(dto.getTagIds()));
        GiftCertificate giftCertificate = findById(dto.getCertificateId());
        for (Tag tag : tagList) {
            if (giftCertificate.getTags().contains(tag)) {
                throw new AlreadyExistsException("Tag with ID " + tag.getId() + " already exists in Gift certificate with ID " + giftCertificate.getId());
            }
            giftCertificate.getTags().add(tag);
        }

        certificateRepository.update(giftCertificate);
        return certificateMapper.toDto(giftCertificate);
    }

    @Override
    @Transactional
    public Boolean deleteTags(Long certificateId, Set<Long> tags) {
        Set<Tag> tagList = tagService.getTagsByIds(new ArrayList<>(tags));
        GiftCertificate giftCertificate = findById(certificateId);

        for (Tag tag : tagList) {
            if (!giftCertificate.getTags().contains(tag)) {
                throw new CustomNotFoundException("Gift Certificate ID: " + giftCertificate.getId() + " does not contain tag ID: " + tag.getId());
            }
        }
        certificateRepository.removeTags(giftCertificate, tagList);
        return true;
    }

    public GiftCertificate findById(Long id) {
        GiftCertificate certificate = certificateRepository.findById(id);
        if (Utils.isEmpty(certificate)) {
            throw new CustomNotFoundException("GiftCertificate with ID: " + id + " is not found");
        }
        return certificate;
    }

    public List<GiftCertificateDTO> checkCertificates(List<GiftCertificate> certificateList) {
        if (Utils.isEmpty(certificateList)) {
            throw new CustomNotFoundException("certificates are not found");
        }
        return certificateMapper.toDtoList(certificateList);
    }

    @Transactional
    public void generateAndSave() {
        certificateRepository.generateAndSave(tagService.getRandomTags());
    }

    public GiftCertificate getRandomCertificate() {
        return certificateRepository.getRandomCertificate();
    }

    private void checkSortOrder(String sortOrder) {
        if (!"asc".equalsIgnoreCase(sortOrder) && !"desc".equalsIgnoreCase(sortOrder)) {
            throw new IllegalArgumentException("Invalid sortOrder. Use 'asc' or 'desc'.");
        }
    }
}
