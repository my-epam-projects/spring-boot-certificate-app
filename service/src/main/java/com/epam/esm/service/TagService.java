package com.epam.esm.service;

import com.epam.esm.dto.tag.TagCreateDTO;
import com.epam.esm.dto.tag.TagDTO;
import com.epam.esm.dto.tag.TagUpdateDTO;
import com.epam.esm.dto.tag.TagWithTotalCostDTO;
import com.epam.esm.entity.Tag;
import com.epam.esm.utils.PageRequest;

import java.util.List;
import java.util.Set;

/**
 * Service interface for managing tags.
 * Defines methods for CRUD operations, searching, and tag-related operations.
 *
 * @author Jaloliddinov
 */
public interface TagService {

    /**
     * Retrieves a paginated list of all tags, with optional filtering and sorting.
     *
     * @param page        Page number to retrieve.
     * @param size        Number of items per page.
     * @param sortField   Field to sort the result by.
     * @param sortOrder   Sort order (asc/desc).
     * @param filterField Field to filter the result by.
     * @param filterValue Value to filter the result by.
     * @return A paginated list of all tags.
     */
    PageRequest<List<TagDTO>> getAll(Integer page, Integer size, String sortField, String sortOrder, String filterField, String filterValue);

    /**
     * Retrieves a tag by its ID.
     *
     * @param id The ID of the tag.
     * @return The tag with the specified ID.
     */
    TagDTO getById(Long id);

    /**
     * Creates a new tag based on the provided DTO.
     *
     * @param dto The DTO containing information for creating the tag.
     * @return The created tag.
     */
    TagDTO create(TagCreateDTO dto);

    /**
     * Updates an existing tag based on the provided DTO and ID.
     *
     * @param dto The DTO containing information for updating the tag.
     * @param id  The ID of the tag to update.
     * @return The updated tag.
     */
    TagDTO update(TagUpdateDTO dto, Long id);

    /**
     * Deletes a tag based on its ID.
     *
     * @param id The ID of the tag to delete.
     * @return True if the deletion was successful, false otherwise.
     */
    Boolean delete(Long id);

    /**
     * Retrieves a set of tags by their IDs.
     *
     * @param ids The list of tag IDs to retrieve.
     * @return The set of tags with the specified IDs.
     */
    Set<Tag> getTagsByIds(List<Long> ids);

    /**
     * Retrieves a list of tags along with the total cost of user orders for each tag.
     *
     * @param userId The ID of the user for whom to retrieve tags and total cost.
     * @return A list of tags with their corresponding total cost.
     */
    List<TagWithTotalCostDTO> getMostWidelyUsedTagOfUser(Long userId);

    /**
     * Generates and saves a set of tags with random data.
     */
    void generateAndSave();

    /**
     * Retrieves a set of random tags.
     *
     * @return A set of randomly selected tags.
     */
    Set<Tag> getRandomTags();
}
