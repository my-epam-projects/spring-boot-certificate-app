package com.epam.esm.service.impl;


import com.epam.esm.dto.tag.TagCreateDTO;
import com.epam.esm.dto.tag.TagDTO;
import com.epam.esm.dto.tag.TagUpdateDTO;
import com.epam.esm.dto.tag.TagWithTotalCostDTO;
import com.epam.esm.entity.Tag;
import com.epam.esm.exceptions.AlreadyExistsException;
import com.epam.esm.exceptions.CustomNotFoundException;
import com.epam.esm.helper.Utils;
import com.epam.esm.mapper.TagMapper;
import com.epam.esm.repository.TagRepository;
import com.epam.esm.service.TagService;
import com.epam.esm.utils.PageRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;
    private final TagMapper tagMapper;

    @Override
    public PageRequest<List<TagDTO>> getAll(Integer page, Integer size, String sortField, String sortOrder, String filterField, String filterValue) {
        if (!"asc".equalsIgnoreCase(sortOrder) && !"desc".equalsIgnoreCase(sortOrder)) {
            throw new IllegalArgumentException("Invalid sortOrder. Use 'asc' or 'desc'.");
        }
        PageRequest<List<Tag>> listPageRequest = tagRepository.findAll(page, size, sortField, sortOrder, filterField, filterValue);
        List<TagDTO> dtoList = checkTagList(listPageRequest.getResultList());
        return new PageRequest<>(listPageRequest.getResultCount(), dtoList);
    }

    @Override
    public TagDTO getById(Long id) {
        Tag tag = findById(id);
        return tagMapper.toDto(tag);
    }

    public List<TagWithTotalCostDTO> getMostWidelyUsedTagOfUser(Long userId) {
        List<TagWithTotalCostDTO> dtoList = tagRepository.findMostWidelyUsedTag(userId)
                .stream()
                .map(tuple -> new TagWithTotalCostDTO(
                        tuple.get("user_id", Long.class),
                        tuple.get("tag_id", Long.class),
                        tuple.get("tag_name", String.class),
                        tuple.get("tag_count", Long.class),
                        tuple.get("total_cost", Long.class)
                ))
                .toList();

        if (Utils.isEmpty(dtoList)) {
            throw new CustomNotFoundException("Tag is not found for User ID: " + userId);
        }

        return dtoList;
    }

    @Override
    @Transactional
    public TagDTO create(TagCreateDTO dto) {
        findByName(dto.getName());
        Tag tag = tagMapper.toEntity(dto);
        tagRepository.save(tag);

        return tagMapper.toDto(tag);
    }

    @Override
    @Transactional
    public TagDTO update(TagUpdateDTO dto, Long id) {
        Tag tag = findById(id);
        findByName(dto.getName());
        tag.setId(id);
        tag.setName(dto.getName());
        tagRepository.update(tag);

        return tagMapper.toDto(tag);
    }

    @Override
    @Transactional
    public Boolean delete(Long id) {
        Tag tag = findById(id);
        tagRepository.delete(tag);
        return true;
    }

    public Set<Tag> getTagsByIds(List<Long> ids) {
        Map<Long, Boolean> map = tagRepository.areIdsValid(ids);
        List<Long> tagIds = map.entrySet().stream()
                .filter(entry -> !entry.getValue())
                .map(Map.Entry::getKey)
                .toList();

        if (Utils.isPresent(tagIds)) {
            throw new CustomNotFoundException("tag IDs " + tagIds + " is not found");
        }
        return new HashSet<>(tagRepository.findByIds(ids));
    }

    public Tag findById(Long id) {
        Tag tag = tagRepository.finById(id);
        if (Utils.isEmpty(tag)) {
            throw new CustomNotFoundException("Tag with ID: " + id + " id not found");
        }
        return tag;
    }

    @Transactional
    public void generateAndSave() {
        tagRepository.generateAndSave();
    }

    public Set<Tag> getRandomTags() {
        return tagRepository.getRandomTagsWithRandomCount();
    }

    private void findByName(String name) throws AlreadyExistsException {
        Tag tag = tagRepository.finByName(name);
        if (Utils.isPresent(tag)) {
            throw new AlreadyExistsException("Tag name: " + name + " is already exists");
        }
    }

    private List<TagDTO> checkTagList(List<Tag> tagList) {
        if (Utils.isEmpty(tagList)) {
            throw new CustomNotFoundException("tags are not found");
        }
        return tagMapper.toDtoList(tagList);
    }

}
