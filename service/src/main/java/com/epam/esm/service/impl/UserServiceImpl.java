package com.epam.esm.service.impl;


import com.epam.esm.dto.token.ReqLoginDTO;
import com.epam.esm.dto.token.ReqRefreshTokenDTO;
import com.epam.esm.dto.token.SessionDTO;
import com.epam.esm.dto.user.UserCreateDTO;
import com.epam.esm.dto.user.UserDTO;
import com.epam.esm.entity.Role;
import com.epam.esm.entity.User;
import com.epam.esm.exceptions.AlreadyExistsException;
import com.epam.esm.exceptions.CustomForbiddenException;
import com.epam.esm.exceptions.CustomNotFoundException;
import com.epam.esm.exceptions.InValidInputException;
import com.epam.esm.helper.Utils;
import com.epam.esm.mapper.UserMapper;
import com.epam.esm.repository.UserRepository;
import com.epam.esm.service.JwtService;
import com.epam.esm.service.RoleService;
import com.epam.esm.service.UserService;
import com.epam.esm.utils.PageRequest;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final RoleService roleService;
    private final AuthenticationManager authenticationManager;

    @Override
    public PageRequest<List<UserDTO>> getAll(Integer page, Integer size, String sortField, String sortOrder, String filterField, String filterValue) {
        if (!"asc".equalsIgnoreCase(sortOrder) && !"desc".equalsIgnoreCase(sortOrder)) {
            throw new IllegalArgumentException("Invalid sortOrder. Use 'asc' or 'desc'.");
        }
        PageRequest<List<User>> pageRequest = userRepository.findAll(page, size, sortField, sortOrder, filterField, filterValue);
        List<UserDTO> dtoList = checkUserList(pageRequest.getResultList());
        return new PageRequest<>(pageRequest.getResultCount(), dtoList);
    }

    @Override
    public UserDTO getById(Long id) {
        User user = findById(id);
        return userMapper.toDto(user);
    }

    public User findById(Long id) {
        User user = userRepository.findById(id);
        if (Utils.isEmpty(user)) {
            throw new CustomNotFoundException("user with id: " + id + " is not found");
        }
        return user;
    }

    @Override
    public SessionDTO login(ReqLoginDTO dto) {
//        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            dto.getUsername(),
                            dto.getPassword()
                    )
            );
//        } catch (BadCredentialsException e) {
//            throw new CustomForbiddenException("username or password is incorrect!!!");
//        }
        User user = findByUsername(dto.getUsername());

        String accessToken = jwtService.generateAccessToken(user);
        String refreshToken = jwtService.generateRefreshToken(user);
        return SessionDTO
                .builder().accessToken(accessToken)
                .refreshToken(refreshToken)
                .user(userMapper.toDto(user))
                .build();
    }

    @Override
    @Transactional
    public SessionDTO register(UserCreateDTO dto) {

        User user = userMapper.toEntity(dto);
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        //checks for unique
        checkUsername(user.getUsername());
        findByEmail(dto.getEmail());
        checkPassword(dto.getPassword());

        Role role = roleService.getById(1L);
        user.setRoles(Set.of(role));

        userRepository.save(user);

        String accessToken = jwtService.generateAccessToken(user);
        String refreshToken = jwtService.generateRefreshToken(user);
        return SessionDTO
                .builder().accessToken(accessToken)
                .refreshToken(refreshToken)
                .user(userMapper.toDto(user))
                .build();
    }


    @Override
    public SessionDTO refreshToken(ReqRefreshTokenDTO dto) {
        String tokenType = jwtService.extractClaim(dto.getRefreshToken(), claims -> claims.get("token_type", String.class));
        if (!"refresh".equals(tokenType)) {
            throw new InValidInputException("Token type is invalid");
        }
        Map<SessionDTO, String> result = jwtService.refreshToken(dto.getRefreshToken());
        SessionDTO session = result.keySet().iterator().next();
        String username = result.get(session);
        User user = findByUsername(username);

        session.setUser(userMapper.toDto(user));
        return session;
    }

    public User findByUsername(String username) {
        User user = userRepository.findByUsername(username);
        if (Utils.isEmpty(user)) {
            throw new CustomNotFoundException("user with username: " + username + " is not found");
        }
        return user;
    }


    @Transactional
    public void generateAndSave() {
        Faker faker = new Faker();
        Role role = roleService.getById(1L);

        for (int i = 0; i < 1000; i++) {
            User user = new User();
            user.setUsername(faker.name().username() + (500 + i));
            user.setEmail(faker.internet().emailAddress() + (500 + i));
            user.setPassword(passwordEncoder.encode(faker.regexify("[a-zA-Z0-9!@#$%^&*()-_=+]+")));

            user.setRoles(Set.of(role));
            userRepository.save(user);
        }
    }

    public User getRandomUser() {
        return userRepository.randomUser();
    }


    private List<UserDTO> checkUserList(List<User> userList) {
        if (Utils.isEmpty(userList)) {
            throw new CustomNotFoundException("users are not found");
        }
        return userMapper.toDtoList(userList);
    }

    private void checkUsername(String username) {
        User user = userRepository.findByUsername(username);
        if (Utils.isPresent(user)) {
            throw new AlreadyExistsException("username: " + username + " is already registered please sign up!!!");
        }
    }

    private void findByEmail(String email) {
        User user = userRepository.findByEmail(email);
        if (Utils.isPresent(user)) {
            throw new AlreadyExistsException("email: " + email + " is already registered please sign up!!!");
        }
    }

    private void checkPassword(String password) {
        User user = userRepository.findByPassword(password);
        if (Utils.isPresent(user)) {
            throw new AlreadyExistsException("password: " + password + " is already exists");
        }
    }
}
