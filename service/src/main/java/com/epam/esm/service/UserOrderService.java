package com.epam.esm.service;

import com.epam.esm.dto.tag.TagWithTotalCostDTO;
import com.epam.esm.dto.user_order.UserOrderCreateDTO;
import com.epam.esm.dto.user_order.UserOrderDTO;
import com.epam.esm.utils.PageRequest;

import java.util.List;

/**
 * Service interface for managing user orders.
 * Defines methods for retrieving, creating, and searching user orders.
 *
 * @author Jaloliddinov
 */
public interface UserOrderService {

    /**
     * Retrieves a user order by its ID.
     *
     * @param id The ID of the user order.
     * @return The user order with the specified ID.
     */
    UserOrderDTO getById(Long id);

    /**
     * Creates a new user order based on the provided DTO.
     *
     * @param dto The DTO containing information for creating the user order.
     * @return The created user order.
     */
    UserOrderDTO create(UserOrderCreateDTO dto);

    /**
     * Retrieves a paginated list of user orders for a specific user, with optional filtering and sorting.
     *
     * @param userId      The ID of the user for whom to retrieve user orders.
     * @param page        Page number to retrieve.
     * @param size        Number of items per page.
     * @param sortField   Field to sort the result by.
     * @param sortOrder   Sort order (asc/desc).
     * @param filterField Field to filter the result by.
     * @param filterValue Value to filter the result by.
     * @return A paginated list of user orders for the specified user.
     */
    PageRequest<List<UserOrderDTO>> getByUser(Long userId, Integer page, Integer size, String sortField, String sortOrder, String filterField, String filterValue);

    /**
     * Retrieves a list of the highest-priced user orders and the most used tag for a specific user.
     *
     * @param userId The ID of the user for whom to retrieve the information.
     * @return A list of tags with their corresponding total cost for the specified user.
     */
    List<TagWithTotalCostDTO> getHighestPriceAndMostUsedTagOfUser(Long userId);

    /**
     * Generates and saves a set of user orders with random data.
     */
    void generateAndSave();
}
