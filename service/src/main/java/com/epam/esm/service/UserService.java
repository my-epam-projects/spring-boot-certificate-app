package com.epam.esm.service;

import com.epam.esm.dto.token.ReqLoginDTO;
import com.epam.esm.dto.token.ReqRefreshTokenDTO;
import com.epam.esm.dto.token.SessionDTO;
import com.epam.esm.dto.user.UserCreateDTO;
import com.epam.esm.dto.user.UserDTO;
import com.epam.esm.entity.User;
import com.epam.esm.utils.PageRequest;

import java.util.List;

/**
 * Service interface for managing users.
 * Defines methods for retrieving, creating, searching, and authenticating users.
 *
 * @author Jaloliddinov
 */
public interface UserService {

    /**
     * Retrieves a user by its ID.
     *
     * @param id The ID of the user.
     * @return The user with the specified ID.
     */
    UserDTO getById(Long id);

    /**
     * Retrieves a user by its ID.
     *
     * @param id The ID of the user.
     * @return The user with the specified ID.
     */
    User findById(Long id);

    /**
     * Retrieves a user by their username.
     *
     * @param username The username of the user.
     * @return The user with the specified username.
     */
    User findByUsername(String username);

    /**
     * Retrieves a paginated list of all users, with optional filtering and sorting.
     *
     * @param page        Page number to retrieve.
     * @param size        Number of items per page.
     * @param sortField   Field to sort the result by.
     * @param sortOrder   Sort order (asc/desc).
     * @param filterField Field to filter the result by.
     * @param filterValue Value to filter the result by.
     * @return A paginated list of all users.
     */
    PageRequest<List<UserDTO>> getAll(Integer page, Integer size, String sortField, String sortOrder, String filterField, String filterValue);

    /**
     * Generates and saves a set of users with random data.
     */
    void generateAndSave();

    /**
     * Retrieves a random user from the database.
     *
     * @return A randomly selected user.
     */
    User getRandomUser();

    /**
     * Registers a new user based on the provided DTO.
     *
     * @param dto The DTO containing information for registering the user.
     * @return The session details for the registered user.
     */
    SessionDTO register(UserCreateDTO dto);

    /**
     * Authenticates a user based on the provided login DTO.
     *
     * @param dto The DTO containing login credentials.
     * @return The session details for the authenticated user.
     */
    SessionDTO login(ReqLoginDTO dto);

    /**
     * Refreshes an access token using a refresh token.
     *
     * @param dto The DTO containing the refresh token.
     * @return The session details with the updated access token.
     */
    SessionDTO refreshToken(ReqRefreshTokenDTO dto);
}
