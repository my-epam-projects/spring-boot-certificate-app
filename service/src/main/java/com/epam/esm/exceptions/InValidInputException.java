package com.epam.esm.exceptions;

public class InValidInputException extends RuntimeException {

    public InValidInputException(String message) {
        super(message);
    }
}
