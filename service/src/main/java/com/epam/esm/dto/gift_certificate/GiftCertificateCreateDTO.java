package com.epam.esm.dto.gift_certificate;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GiftCertificateCreateDTO {

    @NotBlank(message = "name is required")
    @JsonProperty("name")
    private String name;

    @NotBlank(message = "description is required")
    @JsonProperty("description")
    private String description;

    @NotNull(message = "Price is required")
    @JsonProperty("price")
    @Min(value = 1000, message = "Price must be greater than or equal to 1000")
    private Integer price;

    @NotNull(message = "duration is required")
    @JsonProperty("duration")
    private Integer duration;

    @NotEmpty(message = "tags are required")
    @JsonProperty("tags")
    private List<Long> tagIds;
}
