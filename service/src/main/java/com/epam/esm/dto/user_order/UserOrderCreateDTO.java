package com.epam.esm.dto.user_order;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserOrderCreateDTO {


    @Min(1)
    @NotNull(message = "Gift certificate Id is required")
    @JsonProperty("gift_certificate_id")
    private Long certificateId;

}
