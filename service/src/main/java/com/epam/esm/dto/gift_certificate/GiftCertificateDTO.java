package com.epam.esm.dto.gift_certificate;

import com.epam.esm.dto.tag.TagDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GiftCertificateDTO extends RepresentationModel<GiftCertificateDTO> {

    @JsonProperty("id")
    private Long id;


    @JsonProperty("name")
    private String name;

    @JsonProperty("description")
    private String description;


    @JsonProperty("price")
    private Integer price;


    @JsonProperty("duration")
    private Integer duration;


    @JsonProperty("create_date")
    private String createDate;


    @JsonProperty("last_update_date")
    private String lastUpdateDate;

    @JsonProperty("tags")
    private List<TagDTO> tagList;
}
