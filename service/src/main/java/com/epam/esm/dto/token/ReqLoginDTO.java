package com.epam.esm.dto.token;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class ReqLoginDTO {

    @NotBlank(message = "username is required")
    @JsonProperty("username")
    private String username;

    @NotBlank(message = "password is required")
    @JsonProperty("password")
    private String password;
}
