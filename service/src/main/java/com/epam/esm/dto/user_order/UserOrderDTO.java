package com.epam.esm.dto.user_order;

import com.epam.esm.dto.gift_certificate.GiftCertificateDTO;
import com.epam.esm.dto.user.UserDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserOrderDTO extends RepresentationModel<UserOrderDTO> {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("user")
    private UserDTO user;

    @JsonProperty("certificate")
    private GiftCertificateDTO certificate;

    @JsonProperty("cost")
    private Integer cost;

    @JsonProperty("purchase_time")
    private String purchaseTime;
}
