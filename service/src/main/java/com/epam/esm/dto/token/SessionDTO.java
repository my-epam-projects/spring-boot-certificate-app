package com.epam.esm.dto.token;

import com.epam.esm.dto.user.UserDTO;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SessionDTO {


    private String accessToken;

    private String refreshToken;

    private UserDTO user;
}
