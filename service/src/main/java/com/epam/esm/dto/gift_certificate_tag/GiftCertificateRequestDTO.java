package com.epam.esm.dto.gift_certificate_tag;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GiftCertificateRequestDTO {

    @Min(value = 1, message = "certificate Id cannot be lass than 1")
    @NotNull(message = "certificate id is required")
    @JsonProperty("certificate_id")
    private Long certificateId;

    @NotNull(message = "tags are required")
    @JsonProperty("tags")
    private Set<Long> tagIds;
}
