package com.epam.esm.dto.token;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqRefreshTokenDTO {

    @NotBlank(message = "refresh_token is required")
    @JsonProperty("refresh_token")
    private String refreshToken;
}
