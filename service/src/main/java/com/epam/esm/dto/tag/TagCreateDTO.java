package com.epam.esm.dto.tag;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * (DTO) representing the creation of a new tag.
 * It contains information required to create a new tag.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TagCreateDTO {

    @NotBlank(message = "name is required")
    @JsonProperty("name")
    private String name;
}
